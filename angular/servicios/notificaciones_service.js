var app = angular.module('futbolinApp.notificaciones',[]);

app.factory('Notificaciones', ['$http', '$q', function($http, $q){

	var self = {

		notificaciones:[{
			icono:"fa-user",
			notificacion: "Nuevo usuario registrado"
		},
		{
			icono:"fa-save",
			notificacion: "Utilizando el 50% del disco duro"
		}]
	};

	return self;

}]);
