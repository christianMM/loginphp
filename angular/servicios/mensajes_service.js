var app = angular.module('futbolinApp.mensajes',[]);

app.factory('Mensajes', ['$http', '$q', function($http, $q){

	var self = {

		mensajes:[{
			img:"dist/img/user.jpg",
			nombre:"chris",
			mensaje:"Lorem ipsum Velit adipisicing tempor",
			fecha:'5-marzo'
		},
		{
			img:"dist/img/user.jpg",
			nombre:"Oscar",
			mensaje:"Lorem ipsum In eiusmod reprehenderit reprehenderit.",
			fecha:'8-marzo'
		},
		{
			img:"dist/img/user.jpg",
			nombre:"Tamy",
			mensaje:"Lorem ipsum Sit in pariatur adipisicing Excepteur.",
			fecha:'15-abril'
		},
		{
			img:"dist/img/user.jpg",
			nombre:"3r puesto",
			mensaje:"Valencia",
			fecha:'15 puntos'
		}]

	};

	return self;

}]);
