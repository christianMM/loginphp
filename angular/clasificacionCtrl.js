app.controller('clasificacionCtrl', ['$scope','$http', function($scope,$http){
	$('.mainApp').removeClass('fluid-container').addClass('container');
	
	$scope.setActive("mClasificacion");

	$scope.alumnos={};
	$scope.posicion=5;

	$http.get('php/servicios/equipos.listado.php')
	.success(function(data){

		$scope.alumnos=data;
	});

	$scope.siguientes = function(){

		if ($scope.alumnos.length > $scope.posicion) {

			$scope.posicion +=5;
		};
	}

	$scope.anteriores = function(){

		if ($scope.posicion > 5) {

			$scope.posicion -=5;
		};
	}
	
	// setTimeout(function(){
	// 	$('.punt').addClass('animated bounceOutMM slow');
		
	// 		setTimeout(function(){
			
	// 			$('.hidden').removeClass('hidden').prev().remove();
		
	// 		}, 4000);
	
	// }, 1000);
setTimeout(spinner, 2500);
	
}]);

app.controller('clasificacionACtrl', ['$scope','$http', function($scope,$http){
	
	$scope.setActive("mClasificacionA");

	$scope.alumnos={};
	// $scope.teams={};
	$scope.posicion=5;
	$scope.grupo = 'A';
	$http.get('php/servicios/equipos.listadoA.php')
	.success(function(data){

		$scope.alumnos=data;
	});

	// $http.get('php/servicios/equipos.getEquipoA.php')
	// .success(function(data){

	// 	if (data.err !== undefined) {
	// 		window.location  = "#/";
	// 		return;
	// 	}
	// 	$scope.teams = data;

	// });

	$scope.siguientes = function(){

		if ($scope.alumnos.length > $scope.posicion) {

			$scope.posicion +=5;
		};
	}

	$scope.anteriores = function(){

		if ($scope.posicion > 5) {

			$scope.posicion -=5;
		};
	}

setTimeout(spinner, 2500);

}]);

app.controller('clasificacionBCtrl', ['$scope','$http', function($scope,$http){
	
	$scope.setActive("mClasificacionB");

	$scope.alumnos={};
	$scope.posicion=5;
	$scope.grupo = 'B';


	$http.get('php/servicios/equipos.listadoB.php')
	.success(function(data){

		$scope.alumnos=data;
	});

	$scope.siguientes = function(){

		if ($scope.alumnos.length > $scope.posicion) {

			$scope.posicion +=5;
		};
	}

	$scope.anteriores = function(){

		if ($scope.posicion > 5) {

			$scope.posicion -=5;
		};
	}
setTimeout(spinner, 2500);

}]);

app.controller('clasificacionCCtrl', ['$scope','$http', function($scope,$http){
	
	$scope.setActive("mClasificacionC");

	$scope.alumnos={};
	$scope.posicion=5;
	$scope.grupo = 'C';

	$http.get('php/servicios/equipos.listadoC.php')
	.success(function(data){

		$scope.alumnos=data;
	});

	$scope.siguientes = function(){

		if ($scope.alumnos.length > $scope.posicion) {

			$scope.posicion +=5;
		};
	}

	$scope.anteriores = function(){

		if ($scope.posicion > 5) {

			$scope.posicion -=5;
		};
	}
setTimeout(spinner, 2500);

}]);

app.controller('clasificacionDCtrl', ['$scope','$http', function($scope,$http){
	
	$scope.setActive("mClasificacionD");

	$scope.alumnos={};
	$scope.posicion=5;
	$scope.grupo = 'D';

	$http.get('php/servicios/equipos.listadoD.php')
	.success(function(data){

		$scope.alumnos=data;
	});

	$scope.siguientes = function(){

		if ($scope.alumnos.length > $scope.posicion) {

			$scope.posicion +=5;
		};
	}

	$scope.anteriores = function(){

		if ($scope.posicion > 5) {

			$scope.posicion -=5;
		};
	}

	
setTimeout(spinner, 2500);
	

}]);

app.controller('usersCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.users = {};

    $http.get('php/servicios/getUsers.php')
        .success(function(data) {

            $scope.users = data;
        });

    setTimeout(registeredUsers, 500);
    setTimeout(spinner, 1500);

}]);
