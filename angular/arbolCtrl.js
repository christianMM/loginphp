app.controller('arbolCtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;

    $http.get('php/servicios/resultados.getGrupoA.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
       $(document).ready(function(){ var team = $('.m_segment'); var loser = $('.loser'); var roundOf16 = $('.r_16'); var roundOf8 = $('.r_8'); var roundOf4 = $('.r_4'); var final = $('.r_2'); team.hover(function(){ var $this = $(this); var winnderId = $this.attr("data-team-id"); var $teams = $("[data-team-id="+winnderId+"]"); $teams.toggleClass('highlight'); $teams.parent().toggleClass('highlight'); });  loser.hover(function(){ $(this).parent().removeClass('highlight'); }); roundOf16.hover(function(){ roundOf16.toggleClass('focus'); }); roundOf8.hover(function(){ roundOf8.toggleClass('focus'); }); roundOf4.hover(function(){ roundOf4.toggleClass('focus'); }); final.hover(function(){ $(this).toggleClass('focus'); }); }); 
}]);