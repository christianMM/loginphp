 var app = angular.module('futbolinApp', [
 	'ngRoute',
 	'futbolinApp.configuracion',
 	'futbolinApp.mensajes',
 	'futbolinApp.notificaciones'
 	] );


 app.controller('mainCtrl', ['$scope','$http','Configuracion','Mensajes','Notificaciones', function ($scope,$http,Configuracion,Mensajes,Notificaciones) {

	$scope.menuSuperior = 'template/menu.html';
 	$scope.config = {};
 	$scope.mensajes = Mensajes.mensajes;
 	$scope.notificaciones = Notificaciones.notificaciones;
 	// console.log($scope.notificaciones);

	$scope.setActive = function(Opcion){

		$scope.mInicio     = "";
		$scope.mProfesores = "";
		$scope.mClasificacion     = "";
		$scope.mClasificacionA    = "";
		$scope.mClasificacionB    = "";
		$scope.mClasificacionC    = "";
		$scope.mClasificacionD    = "";
		$scope.mClasificacion     = "";
		$scope.mresultadoA  	  = "";
		$scope.mresultadoB   	  = "";
		$scope.mresultadoC   	  = "";
		$scope.mresultadoD   	  = "";
		$scope.mresultadoDieciseisavos   = "";
		$scope.mresultadoOcatvos   		 = "";
		$scope.mresultadoCuartos  	     = "";
		$scope.mresultadoSemifinales     = "";
		$scope.mresultadoFinal   		 = "";
		$scope.mresultadoIdaTercer   	 = "";
		$scope.mresultadoVueltaTercer	 = "";

		$scope[Opcion] = "active";

	}

 	$scope.usuario = {
 		nombre: "Christian Llansola"
 	}


 	Configuracion.cargar().then( function(){
 		$scope.config= Configuracion.config;
 	});
 	
 }]);

/////
// Rutas //
/////

 app.config([ '$routeProvider', function($routeProvider){


$routeProvider
	.when('/', {
		templateUrl: 'dashboard/dashboard.html'
	})
	.when('/clasificacion/A', {
		templateUrl: 'dashboard/clasGrupos.html',
		controller: 'clasificacionACtrl'
	})
	.when('/clasificacion/B', {
		templateUrl: 'dashboard/clasGrupos.html',
		controller: 'clasificacionBCtrl'
	})
	.when('/clasificacion/C', {
		templateUrl: 'dashboard/clasGrupos.html',
		controller: 'clasificacionCCtrl'
	})
	.when('/clasificacion/D', {
		templateUrl: 'dashboard/clasGrupos.html',
		controller: 'clasificacionDCtrl'
	})
	.when('/clasificacion/', {
		templateUrl: 'dashboard/clasificacion.html',
		controller: 'clasificacionCtrl'
	})
	.when('/resultados/GrupoA', {
		templateUrl: 'dashboard/resultado-A.html',
		controller: 'resultadoGrupoACtrl'
	})
	.when('/resultados/GrupoB', {
		templateUrl: 'dashboard/resultado-B.html',
		controller: 'resultadoGrupoBCtrl'
	})
	.when('/resultados/GrupoC', {
		templateUrl: 'dashboard/resultado-C.html',
		controller: 'resultadoGrupoCCtrl'
	})
	.when('/resultados/GrupoD', {
		templateUrl: 'dashboard/resultado-D.html',
		controller: 'resultadoGrupoDCtrl'
	})
	.when('/proximos/', {
		templateUrl: 'dashboard/proximos.html',
		controller: 'proximosCtrl'
	})
	.when('/arbol/', {
		templateUrl: 'dashboard/arbolEliminatorias.html',
		controller: 'arbolCtrl'
	})
	.when('/resultados/tercer-y-cuarto', {
		templateUrl: 'dashboard/resultado-tercer.html',
		controller: 'resultadosTercerCtrl'
	})
	.when('/users', {
		templateUrl: 'dashboard/newUsers.html',
		controller: 'usersCtrl'
	})
	.otherwise({
		redirectTo: '/'
	})

 }]);

 /////
// Filtros //
/////


app.filter('quitarletra', function(){
	return function(palabra){
		if (palabra) {
			if (palabra.length > 1) 
				return palabra.substr(1);
			else
				return palabra;
		}
	}
})

.filter('mensajecorto', function(){
	return function(mensaje){
		if (mensaje) {
			if (mensaje.length > 20) 
				return mensaje.substr(0,35) + '...';
			else
				return mensaje;
		}
	}
})



function spinner(){

  $('.row.loader').addClass('hidden').next().removeClass('hidden').prev().remove();
	// $('.row.hidden')removeClass('hidden');
};

function sumaTercer(){
	var team1= $('.totales p > span').data('local');
	var escudo1=$('.totales p > span').data('img');
	var escudo2=$('.totales p > span + span').data('img');
	var team2= $('.totales p > span + span').data('visitante');
	var local1= parseInt($('.totales p > span').first().text());
	var local2 = parseInt($('.totales p + p > span').first().next().text());
	var totalLocal = local1+local2;

	var visitante1= parseInt($('.totales p + p > span').first().text());
	var visitante2 = parseInt($('.totales p > span').first().next().text());
	var totalVisitante = visitante1+visitante2;
	// var totalLocal = parseInt($('.totales p > span').first().text())+parseInt($('.totales p > span').first().next().text());

     // console.log(team1 + ' : ' + totalLocal + ' / ' +team2 + ' : ' + totalVisitante);


      if (totalLocal > totalVisitante) {
      	$('h5.tercer').html(team1);
      	$('img.ganador').attr('src',escudo1);
      	$('h5.cuarto').html(team2);
      	$('img.perdedor').attr('src',escudo2);
      }else{
      	$('h5.tercer').html(team2);
      	$('img.ganador').attr('src',escudo2);
      	$('h5.cuarto').html(team1);
      	$('img.perdedor').attr('src',escudo1);
      }
      
   $('.col-md-4.hidden.totales').remove();
   $('.finalistas').remove();
}

function registeredUsers () {
	     var contador = $('.users-list li').length;
    $('.registeredUsers').text(contador + ' usuarios registrados');
window.clear;
}