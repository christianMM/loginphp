app.controller('resultadoGrupoACtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;

    $http.get('php/servicios/resultados.getGrupoA.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
}]);

app.controller('resultadoGrupoBCtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;

    $http.get('php/servicios/resultados.getGrupoB.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
}]);

app.controller('resultadoGrupoCCtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;

    $http.get('php/servicios/resultados.getGrupoC.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
}]);
app.controller('resultadoGrupoDCtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;

    $http.get('php/servicios/resultados.getGrupoD.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
}]);

app.controller('proximosCtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;

    $http.get('php/servicios/resultados.getGrupoA.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
}]);

app.controller('resultadosTercerCtrl', ['$scope', '$http', function($scope, $http) {


    $scope.resultados = {};
    $scope.posicion = 5;
    $scope.totalGolesT1 = 0;
    $scope.totalGolesT2 = 0;

    $http.get('php/servicios/resultados.getTercer.php')
        .success(function(data) {

            $scope.resultados = data;
        });

    $scope.siguientes = function() {

        if ($scope.resultados.length > $scope.posicion) {

            $scope.posicion += 5;
        };
    }

    $scope.anteriores = function() {

        if ($scope.posicion > 5) {

            $scope.posicion -= 5;
        };
    }

    setTimeout(spinner, 2500);
    setTimeout(sumaTercer, 100);
}]);