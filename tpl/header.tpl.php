<header class="main-header">
  <a href="app.php?node=home" class="logo">
    <span class="logo-mini">
      <img class="img-responsive" src="img/main-logo.png" alt="main-logo.png">
    </span>
    <span class="logo-lg">
      <img class="img-responsive" src="img/small-logo.png" alt="small-logo.png">
    </span>
  </a>
  <nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown notifications-menu">
          <a href="mailto:hq.spain.ito.webmasters@media-saturn.com?subject=Message from MMPT Panel ">
            <i class="fa fa-life-ring"></i>
          </a>
        </li>
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="img/user-<?php print $_SESSION['username']; ?>.jpg" class="user-image" alt="User Image"/>
            <span class="hidden-xs"><?php print $_SESSION['username']; ?></span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="img/user-<?php print $_SESSION['username']; ?>.jpg" class="img-circle" alt="User Image" />
              <p>
                <?php print $_SESSION['username']; ?> - <?php print ($_SESSION['username'] == 'Joan') ? 'Web Developer' : 'Administrador';?>
                <small>Joined 2015/12/30</small>
              </p>
            </li>
            <li class="user-footer">
              <div class="pull-right">
                <a href="inc/logout.php" class="btn btn-default btn-flat">Logout</a>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <a href="inc/logout.php"><i class="fa fa-power-off"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>