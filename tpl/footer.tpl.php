<footer class="main-footer">
	<div class="pull-right hidden-xs">
		Developed by MS Webmaster Team
	</div>
	<strong>Copyright &copy; <?php print date('Y'); ?> Media-Saturn Iberia.</strong> All rights reserved.
</footer>