<!DOCTYPE html>
<html>
  <head>
    <?php require_once 'tpl/head.tpl.php'; ?>
  </head>
  <body class="error-403-page">
    <div id="bg">
      <canvas></canvas>
      <canvas></canvas>
      <canvas></canvas>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
          <div class="login-box">
            <div class="login-logo">
              <a href="/panel/index.php">
                <img src="img/logo-login.png" alt="logo.png">
              </a>
            </div>
            <div class="error-403-body">
              <header>
                <i class="fa fa-4x fa-exclamation-triangle"></i>
                <h3 class="text-center">
                  Error 403
                </h3>
              </header>
                <p class="text-center">
                  Something went wrong or your session has expired.<br />
                  Please, log in again.
                </p>
                <a class="btn btn-default btn-xs center-block" href="/panel/index.php">Back to the login page</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require_once 'tpl/scripts.tpl.php'; ?>
  </body>
</html>