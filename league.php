<?php
  include_once 'inc/db_connect.php';
  include_once 'inc/functions.php';

  sec_session_start();

  if (login_check($mysqli) == true)
  {
    $logged = 'Logged in';
  }
  else {
    $logged = 'Logged out';
    header('Location: index.php');
  }
?>
<!DOCTYPE html>
<html ng-app="futbolinApp" ng-controller="mainCtrl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config.aplicacion}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="dist/css/skins/skin-red.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Import angular -->
    <script type="text/javascript" src="angular/lib/angular.min.js"></script>
    <script type="text/javascript" src="angular/lib/angular-route.min.js"></script>

    <!-- controladores -->
    <script type="text/javascript" src="angular/app.js"></script>
    <script type="text/javascript" src="angular/servicios/configuracion_service.js"></script>
    <script type="text/javascript" src="angular/servicios/mensajes_service.js"></script>
    <script type="text/javascript" src="angular/servicios/notificaciones_service.js"></script>
    <script type="text/javascript" src="angular/clasificacionCtrl.js"></script>
    <script type="text/javascript" src="angular/resultadosCtrl.js"></script>
    <script type="text/javascript" src="angular/arbolCtrl.js"></script>
    <style>
    [ng-cloak] {
      display: none;
    }
  </style>
  </head>

  <body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="/league.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>{{ config.inciales[0] }}</b>{{ config.inciales | quitarletra }}</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>{{config.aplicacion}} </b>{{config.inciales}}</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!-- <li class="dropdown messages-menu" ng-include="'template/mensajes.html'"></li> -->

           


              <!-- Notifications Menu -->
              <!-- <li class="dropdown notifications-menu" ng-include="'template/notificaciones.html'"></li> -->
 
              <!-- User Account Menu -->
              <!-- <li class="dropdown user user-menu" ng-include="'template/usuario.html'"></li> -->

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" ng-include="'template/menu.html'"></aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Semana del 1 al 5 de Abril del 2016
  </h1>

</section>

        <!-- Main content -->
        <section class="content" ng-view>

          <!-- Your Page Content Here -->


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          {{config.version}}
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{config.anio}} <a href="{{config.web}}">{{config.empresa}}</a>.</strong> Derechos reservados.
      </footer>

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="angular/lib/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
