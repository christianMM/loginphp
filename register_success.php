<!DOCTYPE html>
<html>
  <head>
    <?php require_once 'tpl/head.tpl.php'; ?>
  </head>
  <body class="register-page">
    <div id="bg">
      <canvas></canvas>
      <canvas></canvas>
      <canvas></canvas>
    </div>
    <div class="container">
      <div class="row">
        <div class="login-logo">
          <img src="img/logo-login.png" alt="logo.png">
        </div>
      </div>
      <div class="login-box-body">
        <div class="row">
          <div class="col-xs-12 text-center">
            <header>
              <h3>
                Ya estás inscrito!
              </h3>
            </header>
            <p>
              Tu cuenta ha sido creada correctamente.<br />
              Ya puedes entrar y prepararte para el juego.
              <br><br>
              <a class="btn btn-default btn-sm center-block" style="width: 200px;" href="/league">Accede a Media Champions League</a>
            </p>
          </div>
      </div>
    </div>    
    <?php require_once 'tpl/scripts.tpl.php'; ?>
  </body>
</html>