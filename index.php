<?php
  include_once 'inc/db_connect.php';
  include_once 'inc/functions.php';

  sec_session_start();

  if (login_check($mysqli) == true)
  {
    $logged = 'Logged in';
  }
  else {
    $logged = 'Logged out';
    
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <?php require_once 'tpl/head.tpl.php'; ?>
  </head>
  <body class="login-page">
    <?php
      if (isset($_GET['error']))
      {
        print '<div class="alert alert-danger alert-dismissable">';
        print   '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
        print    '<h4><i class="icon fa fa-ban"></i> Mensaje del sistema</h4>';
        print    'Ha habido un error. Inténtalo de nuevo.';
        print '</div>';
      }
    ?>
   <audio>
  <source src="bso/lechic.ogg" type="audio/ogg">
  <source src="bso/lechic.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio> 
  <div id="bg"></div>
    <div class="login-box">
      <div class="login-logo">
        <img src="img/uefa.png" alt="Media Champions League" class="img-responsive">
      </div>
      <div class="login-box-body">
        <p class="login-box-msg register-title">Por favor, escriba su correo electrónico y contraseña</p>

        <form action="inc/process_login.php" method="post" name="login_form">
          <div class="form-group has-feedback">
            <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" autofocus autocomplete="on" />
            <i class="fa fa-envelope form-control-feedback"></i>
          </div>
          <div class="form-group has-feedback">
            <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña"/>
            <i class="fa fa-lock form-control-feedback"></i>
          </div>
          <div class="row">   
            <div class="col-xs-6">
              <input type="button" value="Log in" class="btn btn-success btn-block btn-flat" onclick="formhash(this.form, this.form.password);" />
            </div>
            <div class="col-xs-6 pull-right">
              <?php
                if (login_check($mysqli) == true)
                {
                  print '<a class="btn btn-danger btn-block btn-flat" href="inc/logout.php"><i class="fa fa-power-off"></i> Desconectar</a>';
                }
                else 
                {
                  print '<a class="btn btn-primary btn-block btn-flat" href="register.php"><i class="fa fa-unlock-alt"></i> Register</a>';
                }
              ?>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php require_once 'tpl/scripts.tpl.php'; ?>
  </body>
</html>