<?php

/*
* Based on this WikiHow's great tutorial:
* http://www.wikihow.com/Create-a-Secure-Login-Script-in-PHP-and-MySQL
*/

include_once 'config.inc';


/**
 *  The function below handles anything related to the start of the session
 */

function sec_session_start()
{
  // Set a custom session name

  $session_name = 'sec_session_id';
  $secure = false;
  
  // This stops JavaScript being able to access the session id
  
  $httponly = true;
  // Forces sessions to only use cookies
  
  if (ini_set('session.use_only_cookies', 1) === FALSE)
  {
    header("Location: ../error.php?err=No se ha podido iniciar una sesión segura.");
    exit();
  }
  
  // Gets current cookies params
  
  $cookieParams = session_get_cookie_params();
  session_set_cookie_params($cookieParams["lifetime"],
  $cookieParams["path"], 
  $cookieParams["domain"], 
  $secure,
  $httponly);
  
  // Sets the session name to the one set above
  
  session_name($session_name);

  // Start the PHP session

  session_start();  

  // regenerated the session, delete the old one

  session_regenerate_id(true); 
}

/**
 *  The function below handles anything related to the login process
 */

function login($email, $password, $mysqli)
{
  // Using prepared statements means that SQL injection is not possible.
    
  if ($stmt = $mysqli->prepare("SELECT id, username, password, salt FROM registros WHERE email = ? LIMIT 1"))
  {
    // Bind "$email" to parameter

    $stmt->bind_param('s', $email);
    
    // Execute the prepared query

    $stmt->execute();
    $stmt->store_result();

    // Get variables from result
    
    $stmt->bind_result($user_id, $username, $db_password, $salt);
    $stmt->fetch();

    // Hash the password with the unique salt
    
    $password = hash('sha512', $password . $salt);

    if ($stmt->num_rows == 1) 
    {
      // If the user exists we check if the account is locked
      // from too many login attempts 

      if (checkbrute($user_id, $mysqli) == true) 
      {
        // Account is locked 
        // Send an email to user saying their account is locked

        return false;
      }
      else 
      {
        // Check if the password in the database matches
        // the password the user submitted

        if ($db_password == $password)
        {
          // Password is correct!
          // Get the user-agent string of the user.
          
          $user_browser = $_SERVER['HTTP_USER_AGENT'];
    
          // XSS protection as we might print this value
          
          $user_id = preg_replace("/[^0-9]+/", "", $user_id);
          $_SESSION['user_id'] = $user_id;
    
          // XSS protection as we might print this value
          
          $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
          $_SESSION['username'] = $username;
          $_SESSION['login_string'] = hash('sha512', $password . $user_browser);
          
          // Login successful.
          
          return true;
        }
        else
        {
          // Password is not correct
          // We record this attempt in the database
          
          $now = time();
          $mysqli->query("INSERT INTO panel_login_attempts(user_id, time)
            VALUES ('$user_id', '$now')");
          return false;
        }
      }
    }
    else
    {
    // No user exists.
    
    return false;
    }
  }
}

/**
 *  The function below takes care against brutal force login attempts
 *  @return {bool}: True or false, based on the check brute protection procedure.
 */

function checkbrute($user_id, $mysqli) {
  
  // Get timestamp of current time 

  $now = time();

  // All login attempts are counted from the past 2 hours

  $valid_attempts = $now - (2 * 60 * 60);

  if ($stmt = $mysqli->prepare("SELECT time FROM panel_login_attempts WHERE user_id = ? AND time > '$valid_attempts'"))
  {
    $stmt->bind_param('i', $user_id);

    // Execute the prepared query. 

    $stmt->execute();
    $stmt->store_result();

    // If there have been more than 5 failed logins 

    if ($stmt->num_rows > 5)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}

/**
 *  The function below checks users credentials against database and allows login based on this checking
 *  @return {bool}. True or false based on the login checking credentials.
 */

function login_check($mysqli) {

  // Check if all session variables are set 

  if (isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string']))
  {
    $user_id = $_SESSION['user_id'];
    $login_string = $_SESSION['login_string'];
    $username = $_SESSION['username'];

    // Get the user-agent string of the user.

    $user_browser = $_SERVER['HTTP_USER_AGENT'];

    if ($stmt = $mysqli->prepare("SELECT password FROM registros WHERE id = ? LIMIT 1"))
    {

      // Bind "$user_id" to parameter. 

      $stmt->bind_param('i', $user_id);

      // Execute the prepared query

      $stmt->execute();
      $stmt->store_result();

      if ($stmt->num_rows == 1)
      {
        // If the user exists get variables from result.

        $stmt->bind_result($password);
        $stmt->fetch();
        $login_check = hash('sha512', $password . $user_browser);

        if ($login_check == $login_string)
        {
          // Logged In!
          return true;
        }
        else
        {
        // Not logged in 
          return false;
        }
      } 
      else 
      {
      // Not logged in 
        return false;
      } 
    }
    else 
    {
      // Not logged in 
      return false;
    }
  }
  else 
  {
    // Not logged in 
    return false;
  }
}

/**
 *  The function below takes care about url sanitization.
 *  @return $url {string}. The sanitized url.
 */

function esc_url($url)
{
  if ('' == $url)
  {
    return $url;
  }

  $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
  $strip = array('%0d', '%0a', '%0D', '%0A');
  $url = (string) $url;

  $count = 1;

  while ($count)
  {
    $url = str_replace($strip, '', $url, $count);
  }

  $url = str_replace(';//', '://', $url);
  $url = htmlentities($url);
  $url = str_replace('&amp;', '&#038;', $url);
  $url = str_replace("'", '&#039;', $url);

  if ($url[0] !== '/')
  {
    // We're only interested in relative links from $_SERVER['PHP_SELF']
    return '';
  }
  else
  {
    return $url;
  }
}

/**
 *  The function below takes care about the database connection state.
 *  @return {bool}. True or false, based on this checking.
 */

function check_database() {

  global $msqli;

  if (!mysqli_connect_errno($msqli))
  {
    return true;
  }
  else 
  {
    return false;
  }
}

/**
 *  The function below outputs a message to the user after on a certain actions.
 *  @param $type {string}. Defines the message-bar style. Available values are:
 *    'success': (green).
 *    'info': (cyan).
 *    'warning': (orange).
 *    'danger': (red).
 *  @param $message {string}. The message to show in the message's bar.
 */

function alert($type, $message) {

  switch ($type) {
    case 'success':
      $icon = 'check';
    break;
    case 'info':
      $icon = 'info';
    break;
    case 'warning':
      $icon = 'warning';
    break;
    case 'danger':
      $icon = 'ban';
    break;
  }

  $output  =  '<div class="alert alert-' . $type . ' alert-dismissible" role="alert">';
  $output .=    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
  $output .=    '<h4><i class="icon fa fa-' . $icon . '"></i> Mensaje del sistema</h4>';
  $output .=    $message;
  $output .= '</div>';

  print $output;
}
