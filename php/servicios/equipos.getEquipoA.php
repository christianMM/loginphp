<?php
// Incluir la clase de base de datos
include_once("../classes/class.Database.php");

// Retorna un json
header('Content-Type: application/json;');


// Verificar si existe
$sql = "SELECT count(*) as Existe FROM clasificacion WHERE idEquipo = 'A'";
$existe = Database::get_valor_query($sql,"Existe");


if ($existe == 1) {
	// Si existe, imprime el json
	$sql = "SELECT * FROM clasificacion WHERE idEquipo = 'A'";
	echo json_encode( Database::get_Row($sql) );	

}else{

	echo json_encode( array('err'=>true, 'mensaje'=>'Código no existe') );

}


?>