<?php

  /**
  *  Uncomment $allow_register and override it's value to 'TRUE' to allow 
  *  users registration.
  *  To avoid user can register by their own, set the variable to 
  *  it's default value: 'FALSE' or comment line 9 entirely
  *
  */   $allow_register = true;
  /**/ 

  include_once 'inc/register.inc.php';
  include_once 'inc/functions.php';

?>

<!DOCTYPE html>
<html>
  <?php if (isset($allow_register) && $allow_register == true): ?>
    <head>
      <?php require_once 'tpl/head.tpl.php'; ?>
    </head>
    <body class="register-page">
      <div id="bg">
      </div>
      <div class="container">
        <div class="row">
      <div class="login-logo">
        <img src="img/uefa.png" alt="Media Champions League" class="img-responsive">
      </div>
        </div>
        <div class="login-box-body">
          <div class="row">
            <div class="col-xs-12">
              <header>
                <h3 class="register-title">Formulario de registro</h3>
              </header>
              <?php
                if (!empty($error_msg)) {
                  echo $error_msg;
                }
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" method="post" name="registration_form">
                <div class="form-group has-feedback">
                  <input type="text" name="username" id="username" class="form-control" placeholder="Usuario"/>
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="email" name="email" id="email" class="form-control" placeholder="Email"/>
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña"/>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" name="confirmpwd" id="confirmpwd" class="form-control" placeholder="Confirmar contraseña"/>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">   
                  <div class="col-xs-6">
                    <p>
                      <a class="link" href="index.php"><i class="fa fa-chevron-circle-left"></i> Página de inicio de sesión</a>
                    </p>
                  </div>
                  <div class="col-xs-6">
                    <input type="button" value="Regístrame" class="btn btn-success btn-block btn-flat" onclick="return regformhash(this.form, this.form.username, this.form.email, this.form.password, this.form.confirmpwd);" /> 
                  </div>
                </div>
              </form>
              
            </div>
            <div class="col-sm-6">
              <p class="login-box-msg">
                <ul class="messages-register">
                  <li>
                    El <i>usuario</i> sólo puede contener números, letras mayúsculas y minúsculas y caracteres de subrayado.
                  </li>
                  <li>
                    El <i>email</i> debe tener un formato válido.
                  </li>
                  <li>
                    La <i>contraseña</i> debe ser mínimo de 6 carácteres.
                  </li>
                  <li>
                    Las contraseñas también deben tener:
                    <ul>
                      <li>Mínimo de una letra mayúscula (A-Z)</li>
                      <li>Mínimo de una letra minúscula (a-z)</li>
                      <li>Mínimo un número (0-9)</li>
                    </ul>
                  </li>
                  <li>
                    La <i>contraseña</i> y <i>la contraseña de confirmación</i> debe coincidir.
                  </li>
                </ul>
              </p>
            </div>
          </div>
        </div>
      </div>    
      <?php require_once 'tpl/scripts.tpl.php'; ?>
    </body>
  <?php endif; ?>
  <?php if (!isset($allow_register) || $allow_register == false): ?>
    <body>
      La página que estás buscando no está activa. Por favor, póngase en contacto con el administrador del sitio.
    </body>
  <?php endif; ?>
</html>