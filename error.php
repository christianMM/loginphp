<?php
  $error = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING);

  if (! $error) {
    $error = 'Boooh! Ha habido un error del sistema';
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <?php require_once 'tpl/head.tpl.php'; ?>
  </head>
  <body class="login-page error-page">

    <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Mensaje del sistema</h4>
      <?php echo $error; ?>
    </div>

  </body>
</html>